import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.StandardItem;

public class StandartItemTest {
//konstruktor, copy, parametrizovaný test pro equals
    private StandardItem standardItem;

    @ParameterizedTest
    @CsvSource({"1, Name One, 10, Category one, 100",
    "2, Name Two, 20, Category Two, 200",
            "3, Name Three, 30, Category Three, 300"})
    public void testConstructor(int id, String name, float price, String category, int loyaltyPoints) {
        standardItem = new StandardItem(id, name, price, category, loyaltyPoints);

        Assertions.assertEquals(id, standardItem.getID());
        Assertions.assertEquals(name, standardItem.getName());
        Assertions.assertEquals(price, standardItem.getPrice());
        Assertions.assertEquals(category, standardItem.getCategory());
        Assertions.assertEquals(loyaltyPoints, standardItem.getLoyaltyPoints());
    }

    @ParameterizedTest
    @CsvSource({"1, Name One, 10, Category one, 100",
            "2, Name Two, 20, Category Two, 200",
            "3, Name Three, 30, Category Three, 300"})
    public void testCopy(int id, String name, float price, String category, int loyaltyPoints) {
        standardItem = new StandardItem(id, name, price, category, loyaltyPoints);
        Assertions.assertEquals(standardItem, standardItem.copy());
    }

    @ParameterizedTest
    @CsvSource({"1, Name One, 10, Category one, 100, 2, Name Two, 20, Category Two, 200, false",
            "2, Name Two, 20, Category Two, 200, 2, Name Two, 20, Category Two, 200, true",
            "3, Name Three, 30, Category Three, 300, 1, Name One, 10, Category one, 100, false"})
    public void testCopy(int id1, String name1, float price1, String category1, int loyaltyPoints1,
                    int id2, String name2, float price2, String category2, int loyaltyPoints2, boolean expectedResult
    ){
        StandardItem standardItemOne = new StandardItem(id1, name1, price1, category1, loyaltyPoints1);
        StandardItem standardItemTwo = new StandardItem(id2, name2, price2, category2, loyaltyPoints2);

        Assertions.assertEquals(standardItemOne.equals(standardItemTwo), expectedResult);
    }
}
