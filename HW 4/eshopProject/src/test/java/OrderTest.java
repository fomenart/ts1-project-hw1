import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

public class OrderTest {
    //konstruktory - nezapomeňte na null hodnoty
    ShoppingCart cart;
    @BeforeEach
    public void initializeCart() {
        cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "one", 10, "categoryOne", 100));
    }

    //Comment: since int doesn't support null values, I have changed type from
    //int to Integer. Also, I have changed cart initialization by adding a null check
    //since after we try to call a method from null, we get Exceptions
    @ParameterizedTest
    @CsvSource(value = {"one, first, 1", "two, null, 2", "three, third, null",
    "null, null, null"}, nullValues = "null")
    public void testConstructor(String customerName,
                                String customerAddress,
                                Integer state){
        Order orderWithState = new Order(cart, customerName, customerAddress, state);
        Order orderWithoutState = new Order(cart, customerName, customerAddress);

        Order orderWithNullCartWithoutState = new Order(null, customerName, customerAddress);
        Order orderWithNullCart = new Order(null, customerName, customerAddress, state);
    }

}
