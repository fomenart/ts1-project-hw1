import archive.ItemPurchaseArchiveEntry;
import archive.PurchasesArchive;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import shop.Order;
import shop.ShoppingCart;
import shop.StandardItem;

import java.util.ArrayList;
import java.util.HashMap;

public class PurchaseArchiveTest {
    //testy metod printItemPurchaseStatistics,
    // getHowManyTimesHasBeenItemSold a
    // putOrderToPurchasesArchive
    //println (stream)
    //mock orderArchive
    //mock ItemPurchaseArchiveEntry

    //ověřte správné volání konstruktoru
    // při vytvoření ItemPurchaseArchiveEntry
    PurchasesArchive purchasesArchive;

    @BeforeEach
    public void initializePurchaseArchive() {
        ItemPurchaseArchiveEntry itemEntry = getItemEntry();
        purchasesArchive = new PurchasesArchive(getItemArchive(itemEntry), getOrderArchive());
    }

    @Test
    public void testPrintItemPurchaseStatistics() {
        purchasesArchive.printItemPurchaseStatistics();
    }

    @Test
    public void testGetHowManyTimesHasBeenItemSold() {
        int amount = purchasesArchive.getHowManyTimesHasBeenItemSold(new StandardItem(1, "one", 10, "categoryOne", 100));
        Assertions.assertEquals(amount, 1);
    }

    //Comment: I have changed accessibility of ItemPurchaseArchiveEntry
    //since it was impossible to call
    //Since I am initializing ItemPurchaseArchiveEntry before each test, I am leaving
    //this function empty
    @Test
    public void testPurchaseArchiveConstructor() {}


    private ItemPurchaseArchiveEntry getItemEntry() {
        return new ItemPurchaseArchiveEntry(new StandardItem(1, "one", 10, "categoryOne", 100));
    }

    private HashMap<Integer, ItemPurchaseArchiveEntry> getItemArchive(ItemPurchaseArchiveEntry itemEntry) {
        HashMap<Integer, ItemPurchaseArchiveEntry> result = new HashMap<>();
        result.put(1, itemEntry);
        return result;
    }

    private ArrayList<Order> getOrderArchive() {
        Order order = getOrder();
        ArrayList<Order> result = new ArrayList<>();
        result.add(order);
        return result;
    }

    private Order getOrder() {
        ShoppingCart cart = getCart();
        return new Order(cart, "Customer One", "Address One", 1);
    }

    private ShoppingCart getCart() {
        ShoppingCart cart = new ShoppingCart();
        cart.addItem(new StandardItem(1, "one", 10, "categoryOne", 100));
        return cart;
    }
}
