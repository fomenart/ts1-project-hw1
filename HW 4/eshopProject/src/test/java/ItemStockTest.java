import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import shop.StandardItem;
import storage.ItemStock;

public class ItemStockTest {
    //konstruktor, parametrizovaný test pro metody změny počtu
    private StandardItem item;
    private ItemStock itemStock;

    @BeforeEach
    public void initiateItem() {
        item = new StandardItem(1, "one", 10, "categoryOne", 100);
    }

    //Comment: I have changed accessibility of constructor, since it was
    //impossible to access
    @Test
    public void testConstructor() {
        itemStock = new ItemStock(item);
    }

    //Comment: I have changed accessibility of methods, since those were
    //impossible to access, also, I have changes name of "IncreaseItemCount"
    //to "increaseItemCount" to follow the naming conventions
    @ParameterizedTest
    @CsvSource({"3, 5", "4, 23", "35, 12", "124, 4", "1, 0", "0, 1", "8, 9", "9, 23", "1, 5"})
    public void testChangeCount(int add, int remove) {
        itemStock = new ItemStock(item);
        itemStock.increaseItemCount(add);
        itemStock.decreaseItemCount(remove);
        Assertions.assertEquals(add - remove, itemStock.getCount());
    }
}
