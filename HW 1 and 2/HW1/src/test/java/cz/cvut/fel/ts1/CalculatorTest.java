package cz.cvut.fel.ts1;

import com.example.hw1ts1.Calculator;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CalculatorTest {
    private Calculator calculator;

    @BeforeEach
    private void initialize() {
        //ARRANGE
        this.calculator = new Calculator();
    }

    @ParameterizedTest
    @CsvSource({"1, 2, 3", "2, 3, 5", "0, 5, 5"})
    public void checkAddition(double number1, double number2, double result) {
        Assertions.assertEquals(result, calculator.add(number1, number2));
    }

    @ParameterizedTest
    @CsvSource({"5, 2, 3", "8, 3, 5", "10, 5, 5"})
    public void checkSubtraction(double number1, double number2, double result) {
        Assertions.assertEquals(result, calculator.subtract(number1, number2));
    }

    @ParameterizedTest
    @CsvSource({"1, 2, 2", "2, 3, 6", "0, 5, 0"})
    public void checkMultiplication(double number1, double number2, double result) {
        Assertions.assertEquals(result, calculator.multiply(number1, number2));
    }

    @ParameterizedTest
    @CsvSource({"6, 2, 3", "15, 3, 5", "56, 0, 5"})
    public void checkDivision(double number1, double number2, double result) {
        if (number2 != 0) {
            Assertions.assertEquals(result, calculator.divide(number1, number2));
        } else {
            Assertions.assertThrows(ArithmeticException.class, () -> calculator.divide(number1, number2));
        }
    }
}