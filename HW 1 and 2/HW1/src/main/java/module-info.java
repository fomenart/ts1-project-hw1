module com.example.hw1ts1 {
    requires javafx.controls;
    requires javafx.fxml;

    requires org.kordamp.bootstrapfx.core;

    opens com.example.hw1ts1 to javafx.fxml;
    exports com.example.hw1ts1;
}