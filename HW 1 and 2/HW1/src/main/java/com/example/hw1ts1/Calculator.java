package com.example.hw1ts1;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class Calculator {
    public double add(double number1, double number2) {
        return number1 + number2;
    }

    public double subtract(double number1, double number2) {
        return number1 - number2;
    }

    public double multiply(double number1, double number2) {
        return number1 * number2;
    }

    public double divide(double number1, double number2) throws ArithmeticException {
        if (number2 == 0) {
            throw new ArithmeticException("Cannot divide by zero");
        } else {
            return number1 / number2;
        }
    }

    public long factorial(int number) {
        long result = 1;
        while (number != 1) {
            result = result * number;
            number -= 1;
        }
        return result;
    }
}

